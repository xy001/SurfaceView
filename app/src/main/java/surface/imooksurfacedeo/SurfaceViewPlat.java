package surface.imooksurfacedeo;

import android.content.Context;
import android.graphics.Canvas;
import android.util.AttributeSet;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

/**
 * Created by xiangyang on 15/7/6.
 */
public class SurfaceViewPlat extends SurfaceView implements SurfaceHolder.Callback, Runnable {

    private SurfaceHolder mHolder;
    private boolean isRunning;
    private Canvas mCanvas;
    private Thread t;

    public SurfaceViewPlat(Context context) {
        this(context, null);
    }

    @Override
    public void surfaceCreated(SurfaceHolder holder) {
        isRunning = true;
        t = new Thread(this);
        t.start();
    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {

    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {
        isRunning = false;

    }

    public SurfaceViewPlat(Context context, AttributeSet attrs) {
        super(context, attrs);
        mHolder = getHolder();
        mHolder.addCallback(this);
//设置焦点
        setFocusable(true);
        setFocusableInTouchMode(true);
        // 设置常量
        setKeepScreenOn(true);


    }

    @Override
    public void run() {
        while (isRunning) {
            mCanvas = mHolder.lockCanvas();
            try {
                if (mCanvas != null) {
                    draw();
                }
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                //释放canvas
                mHolder.unlockCanvasAndPost(mCanvas);
            }
        }

    }

    private void draw() {

    }


}
