package surface.imooksurfacedeo;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

/**
 * Created by xiangyang on 15/7/6.
 */
public class LuckyPan extends SurfaceView implements SurfaceHolder.Callback, Runnable {

    private SurfaceHolder mHolder;
    private boolean isRunning;
    private Canvas mCanvas;
    private Thread t;

    //抽奖文字
    String[] mStrs = new String[]{"单反相机", "恭喜发财", "iPad", "奥特曼小怪兽服装", "Iphone6", "恭喜发财"};
    //图片
    // TODO
    private int[] mImags = new int[]{};
    //图片颜色
    private int[] mColor = new int[]{0xFFFFC300, 0XFFF17E01, 0XFFFFC300, 0XFFF17E01, 0XFFFFC300, 0XFFF17E01};

    private int mItemCount;

    private Bitmap[] mImageBitMap;

    //盘快的整个范围
    private RectF mRange = new RectF();
    //盘快的直径
    private int mRadius;

    Paint mArcPaint; //绘制盘快的画笔
    Paint mTextPaint; //绘制文本的画笔

    //滚动速度
    double mSpeed;

    // 起始角度  volatile多线程共享数据
    private volatile int mStartAngle;

    boolean isShouldEnd;// 结束标志

    int mCenter;//中心位置

    int mPadding;//取最小的padding

    //  背景
    //TODO
    Bitmap bg = BitmapFactory.decodeResource(getResources(), R.mipmap.ic_launcher);
    //字体大小
    float mTextSize = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SHIFT, 20, getResources().getDisplayMetrics());


    public LuckyPan(Context context) {
        this(context, null);
    }

    @Override
    public void surfaceCreated(SurfaceHolder holder) {
        //初始化paint
        mArcPaint = new Paint();
        mArcPaint.setAntiAlias(true);
        mArcPaint.setDither(true);

        mTextPaint = new Paint();
        mTextPaint.setColor(0Xffffffff);
        mTextPaint.setTextSize(mTextSize);

        //初始化将盘的范围
        mRange = new RectF(mPadding, mPadding, mPadding + mRadius, mPadding + mRadius);
        //初始图片
        mImageBitMap = new Bitmap[mItemCount];
        for (int i = 0; i < mItemCount; i++) {
            mImageBitMap[i] = BitmapFactory.decodeResource(getResources(), mImags[i]);

        }


        isRunning = true;
        t = new Thread(this);
        t.start();
    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {

    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {
        isRunning = false;

    }

    public LuckyPan(Context context, AttributeSet attrs) {
        super(context, attrs);
        mHolder = getHolder();
        mHolder.addCallback(this);
//设置焦点
        setFocusable(true);
        setFocusableInTouchMode(true);
        // 设置常量
        setKeepScreenOn(true);


    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        int width = Math.min(getMeasuredWidth(), getMeasuredHeight());

        mPadding = getPaddingLeft();
        //直径
        mRadius = width - mPadding * 2;
        //中心位置
        mCenter = mRadius / 2;
    }

    @Override
    public void run() {
        while (isRunning) {

            try {

                    long startTime = System.currentTimeMillis();
                    draw();
                    long endTime = System.currentTimeMillis();

                    if (endTime - startTime < 50) {
                        Thread.sleep(50 - (endTime - startTime));
                    }


            } catch (Exception e) {
                e.printStackTrace();
            }
        }

    }

    private void draw() {

        mCanvas = mHolder.lockCanvas();
        try {
            if (mCanvas != null) {
                drawBg();
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (mCanvas != null) {
                //释放canvas
                mHolder.unlockCanvasAndPost(mCanvas);
            }
        }

    }

    /**
     * 绘制背景
     */
    private void drawBg() {

        mCanvas.drawColor(0XFFFFFFFF);
        mCanvas.drawBitmap(bg, null, new Rect(mPadding / 2, mPadding / 2,
                getMeasuredWidth() - mPadding / 2, getMeasuredHeight() - mPadding / 2), null);

    }


}
